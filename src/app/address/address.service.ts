import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppModule } from '../app.module';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})

export class AddressService {
    serverRoot = this.app.BACKEND_URL;
    restaurantID = this.app.RESTAURANT_ID;

    constructor(
        private http: HttpClient,
        private app: AppModule
    ) { }

    getAddress(address_id): Observable<any> {
        const getAddressRoute = '/address/get/';
        const url = this.serverRoot + getAddressRoute + address_id;

        return this.http.get(url);
    }
}
