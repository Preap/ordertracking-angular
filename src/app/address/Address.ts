export class Address {
    id: number;
    number: string;
    street_name: string;
    post_code: number;
    area: string;
    lat: number;
    lng: number;
}
