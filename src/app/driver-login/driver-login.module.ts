import { NgModule} from '@angular/core';
import { CommonModule} from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';

import { DriverLoginRoutingModule } from './driver-login-routing.module';
import { DriverLoginComponent } from './driver-login.component';

@NgModule({
    imports: [CommonModule, DriverLoginRoutingModule, TranslateModule, FormsModule],
    declarations: [DriverLoginComponent]
})

export class DriverLoginModule {}
