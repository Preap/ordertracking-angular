import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppModule } from '../../app.module';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})

export class LoginDriverService {

    serverRoot = this.app.BACKEND_URL;

    constructor(
        private http: HttpClient,
        private app: AppModule
    ) {}

    checkDriverCredentials (phone: string, password: string) {
        const checkCredentialsRoute = '/driver/checkCredentials';
        const url = this.serverRoot + checkCredentialsRoute;

        const body = {
            'phone': phone,
            'password': password
        };

        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type':  'application/json',
            })
        };

        return this.http.post(url, body, httpOptions);
    }
}
