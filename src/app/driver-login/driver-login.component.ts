import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../router.animations';
import {TranslateService} from '@ngx-translate/core';
import {Router} from '@angular/router';
import { AppModule } from '../app.module';

import { LoginDriverService } from './services/loginDriver.service';

@Component({
  selector: 'app-driver-login',
  templateUrl: './driver-login.component.html',
  styleUrls: ['./driver-login.component.scss'],
  animations: [routerTransition()]
})
export class DriverLoginComponent implements OnInit {

  phone: string;
  password: string;
  driver: any;

  constructor(
      private translate: TranslateService,
      private loginService: LoginDriverService,
      private app: AppModule,
      public router: Router
  ) {
    this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de', 'zh-CHS']);
    this.translate.setDefaultLang('en');
    const browserLang = this.translate.getBrowserLang();
    this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de|zh-CHS/) ? browserLang : 'en');
  }

  ngOnInit() {}

  loginDriver() {

    this.loginService.checkDriverCredentials(this.phone, this.password)
        .subscribe(driver => {
          this.app.DRIVER_ID = driver[0].id;
          localStorage.setItem('DRIVER_ID', driver[0].id);
          // console.log(this.driver);
          this.router.navigate(['/driver']); // log in if credentials are correct
        });
  }
}
