import {Component, OnInit} from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Driver } from '../../drivers/Driver';
import { Vehicle } from '../../vehicles/Vehicle';
import { DriversService } from '../../drivers/services/drivers.service';
import { VehiclesService } from '../../vehicles/services/vehicles.service';
import {Order} from '../Order';

@Component({
    selector: 'app-modal',
    templateUrl: './modal.component.html',
    styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

    drivers: any;
    vehicles: any;
    selectedDriver: any;
    selectedVehicle: any;
    selectedOrder: Order;

    closeResult: string;
    constructor(private modalService: NgbModal,
                private driverService: DriversService,
                private vehicleService: VehiclesService) { }

    ngOnInit(): void {
        if (!this.drivers) { this.getDrivers(); }
        if (!this.vehicles) { this.getVehicles(); }
    }

    open(content) {
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }

    getDrivers(): void {
        this.driverService.getDrivers()
            .subscribe(drivers => {
                this.drivers = drivers;
            });
    }

    getVehicles(): void {
        this.vehicleService.getVehicles()
            .subscribe(vehicles => {
                this.vehicles = vehicles;
                console.log(this.vehicles);
            });
    }

    testFunction(): void {
      console.log(this.selectedDriver);
      console.log(this.selectedVehicle);
    }
}
