import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppModule } from '../../../app.module';
import { HttpClient } from '@angular/common/http';
import { DriversService } from '../../drivers/services/drivers.service';
import { VehiclesService } from '../../vehicles/services/vehicles.service';

@Injectable({
    providedIn: 'root'
})
export class FetchDriverAndVehicleService {

    // serverRoot = this.app.BACKEND_URL;
    // restaurantID = this.app.RESTAURANT_ID;
    // getDriversRoute = '/driver/getDrivers/';
    // getVehiclesRoute = '/vehicle/getVehicles/';

    constructor(
        private http: HttpClient,
        private app: AppModule,
        private driversService: DriversService,
        private vehicleService: VehiclesService
    ) { }

    getDrivers(): Observable<any> {
        return this.driversService.getDrivers();
    }

    getVehicles(): Observable<any> {
        return this.vehicleService.getVehicles();
    }
}
