import { NgModule} from '@angular/core';
import { CommonModule} from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { OrdersRoutingModule} from './orders-routing.module';
import { OrdersComponent} from './orders.component';
import { PageHeaderModule} from '../../shared';
import { ModalComponent } from './modal-assign-driver-and-vehicle/modal.component';
import { AgmCoreModule} from '@agm/core';
import { ShowDriverModalComponent } from './deliveringNowTable/showDriverModal.component';

@NgModule({
    imports: [
        CommonModule,
        OrdersRoutingModule,
        PageHeaderModule,
        FormsModule,
        NgbModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyBquC_NQpsUwCqtxsxVGJDJS_o2U4dE3XE'
        })],
    declarations: [OrdersComponent, ModalComponent, ShowDriverModalComponent]
})
export class OrdersModule {}
