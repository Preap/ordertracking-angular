import {Address} from '../../address/Address';
import {Driver} from '../drivers/Driver';
import {Customer} from '../../customer/Customer';

export class Order {
    id: number;
    type: string;
    timestamp: any;
    driver: Driver|null ;
    address: Address;
    status: string;
    customer: Customer;
    items: string;


    constructor(id: number, type: string, timestamp: Date, driver: Driver | null, address: Address, status: string, customer: Customer, items: string) {
        this.id = id;
        this.type = type;
        this.timestamp = Order.getTimestamp(timestamp);
        this.driver = driver;
        this.address = address;
        this.status = status;
        this.customer = customer;
        this.items = items;
    }

    private static getTimestamp(timestamp) {
        const date = timestamp.date.split(' ');
        const hour = date[1].slice(0, 3);
        const minute = date[1].slice(3, 5);
        return String(hour + minute);
    }
}
