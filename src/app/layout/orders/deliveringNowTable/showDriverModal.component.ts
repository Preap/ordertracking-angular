import { Component, OnInit, OnDestroy} from '@angular/core';
import { routerTransition} from '../../../router.animations';
import {Driver} from '../../drivers/Driver';
import { DriversService } from '../../drivers/services/drivers.service';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {OrdersComponent} from '../orders.component';

@Component({
    selector: 'app-delivering-now-table',
    templateUrl: './showDriverModal.component.html',
    styleUrls: ['./showDriverModal.component.scss'],
    animations: [routerTransition()]
})

export class ShowDriverModalComponent extends OrdersComponent {

    closeResult: string;
    initialLat = 35.138493;
    initialLng = 33.356504;
    driverLat = 35.138493;  // initial lat
    driverLng = 33.356504;  // initial lng
    zoom = 14;        // initial zoom
    currentPositionLat: number;
    currentPositionLng: number;
    selectedDriver: Driver;

    openMapModal(showDriverModal: HTMLElement, driver: Driver) {
        this.driverService.getDriver(driver.id)
            .subscribe((response: Driver) => {
                this.selectedDriver = new Driver(response.id, response.firstName, response.lastName,
                    response.phone, response.status, response.availability,
                    response.currentLatitude, response.currentLongitude);
                console.log(this.selectedDriver);
                this.driverLat = this.selectedDriver.currentLatitude;
                this.driverLng = this.selectedDriver.currentLongitude;
            });
        console.log(this.driverLat);
        console.log(this.driverLng);
        this.modalService.open(showDriverModal).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${OrdersComponent.getDismissReason(reason)}`;
        });
    }

}
