import { Component, OnInit, OnDestroy} from '@angular/core';
import { routerTransition} from '../../router.animations';
import { Order} from './Order';
import { OrdersService} from './services/orders.service';
import { VehiclesService } from '../vehicles/services/vehicles.service';
import { DriversService } from '../drivers/services/drivers.service';
import { AddressService} from '../../address/address.service';
import { AutomaticAssignmentService } from './services/automaticAssignment.service';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Driver} from '../drivers/Driver';
import {Vehicle} from '../vehicles/Vehicle';
import {DriverViewOrdersComponent} from '../../driver-layout/driver-view-orders/driver-view-orders.component';

@Component({
    selector: 'app-orders',
    templateUrl: './orders.component.html',
    styleUrls: ['./orders.component.scss'],
    animations: [routerTransition()]
})

export class OrdersComponent implements OnInit, OnDestroy {
    private shownOrder: Order;
    phoneNumber: number;

    constructor(
        private orderService: OrdersService,
        protected modalService: NgbModal,
        protected driverService: DriversService,
        private vehicleService: VehiclesService,
        private addressService: AddressService,
        private autoAssignService: AutomaticAssignmentService,
    ) {}

    private orders: Order[] = [];
    closeResult: string;
    drivers: Driver[] = [];
    vehicles: Vehicle[] = [];
    selectedDriver: Driver;
    selectedDriverID: any;
    selectedVehicle: any;
    selectedOrder: any;
    automaticAssignment = false;
    deliveringOrders: Order[] = [];
    availableDrivers: Driver[] = [];
    unassignedOrders: Order[] = [];

    // MapModal variables
    initialLat = 35.138493;
    initialLng = 33.356504;
    driverLat = 35.138493;  // initial lat
    driverLng = 33.356504;  // initial lng
    zoom = 14;        // initial zoom
    currentPositionLat: number;
    currentPositionLng: number;

    static parseWord(word) {
        const result = word.split(' ');
        return result[result.length - 1];
    }

     static getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }

    ngOnInit() {
        // Start timer every seconds
        this.getOrders();
        this.getVehicles();
        this.getDrivers();

        window.navigator.geolocation.getCurrentPosition((response) => {
            // console.log(response);
            this.currentPositionLat = response.coords.latitude;
            this.currentPositionLng = response.coords.longitude;
        });

        // Get drivers and vehicles list once!
        // if (!this.drivers) { this.getDrivers(); }
        // if (!this.vehicles) { this.getVehicles(); }
    }

    ngOnDestroy() {
        // Stop timer
    }

    getOrders(): void {
        this.orders = [];
        this.orderService.getOrders()
            .subscribe((orders: Order[]) => {
                for (const order of orders) {
                    const nextOrder = new Order(order.id, order.type, order.timestamp, order.driver,
                                                order.address, order.status, order.customer, order.items);
                    this.orders.push(nextOrder);
                }
                this.sortDeliveringOrders();
                this.sortUnassignedOrders();
            });
    }

    getAddress(address_id): void {
        this.addressService.getAddress(address_id)
            .subscribe(address => console.log(address));
    }

    open(content, orderId) {
        this.selectedOrder = orderId;
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${OrdersComponent.getDismissReason(reason)}`;
        });
    }

    getDrivers(): void {
        this.drivers = [];
        this.driverService.getDrivers()
            .subscribe((drivers: Driver[]) => {
                for (const driver of drivers) {
                    const nextDriver = new Driver(driver.id, driver.firstName, driver.lastName,
                                                    driver.phone, driver.status, driver.availability,
                                                    driver.currentLatitude, driver.currentLongitude);
                    this.drivers.push(nextDriver);
                    // console.log(nextDriver);
                }
                this.selectedDriver = this.drivers[0];
                this.sortAvailableDrivers();
            });
    }

    getVehicles(): void {
        this.vehicles = [];
        this.vehicleService.getVehicles()
            .subscribe((vehicles: Vehicle[]) => {
                for (const vehicle of vehicles) {
                    const nextVehicle = new Vehicle(
                        vehicle.id,
                        vehicle.type,
                        vehicle.currentCapacity,
                        vehicle.maxCapacity,
                        vehicle.availability,
                        vehicle.status,
                        vehicle.driver);
                    this.vehicles.push(nextVehicle);
                }
            });
    }

    assignOrder(): void {
      // console.log('assignOrder called');
      this.selectedVehicle = OrdersComponent.parseWord(this.selectedVehicle);
      this.selectedDriverID = OrdersComponent.parseWord(this.selectedDriverID);
      this.driverService.getDriver(this.selectedDriverID)
          .subscribe((driver: Driver) => {
             this.selectedDriver = new Driver(driver.id, driver.firstName, driver.lastName,
                                             driver.phone, driver.status, driver.availability,
                                             driver.currentLatitude, driver.currentLongitude);
          });
      this.orderService.assignOrder(this.selectedOrder, this.selectedVehicle, this.selectedDriverID)
        .subscribe(response => console.log(response));
      this.getOrders();
    }

    async toggleAutomaticAssignment() {
        this.automaticAssignment = !this.automaticAssignment;
        if (this.automaticAssignment) {
            this.autoAssignService.assignOrders(this.unassignedOrders, this.availableDrivers, this.vehicles);
            await this.delay(3000);
            this.getDrivers();
            this.getOrders();
            if (this.unassignedOrders.length !== 0 && this.availableDrivers.length !== 0) {
                this.automaticAssignment = false;
                this.toggleAutomaticAssignment();
            }
        }
    }

    sortDeliveringOrders(): void {
        this.deliveringOrders = [];
        for (const order of this.orders) {
            if (order.status === 'delivering') {
                this.deliveringOrders.push(order);
            }
            this.orders = this.orders.filter(function (value, index, arr) {
                if (value.status !== 'delivering') {
                    return value;
                }
            });
        }
    }

    sortUnassignedOrders(): void {
        this.unassignedOrders = [];
        for (const order of this.orders) {
            if (order.driver === null ) {
                this.unassignedOrders.push(order);
            }
        }
    }

    sortAvailableDrivers(): void {
        this.availableDrivers = [];
        for (const availableDriver of this.drivers) {
            if (availableDriver.availability === true ) {
                this.availableDrivers.push(availableDriver);
            }
        }
    }

    delay(ms: number) {
        return new Promise( resolve => setTimeout(resolve, ms) );
    }

    openOrderModalData(content, order) {
        this.shownOrder = order;
        this.phoneNumber = parseInt(order.customer.phone);
        console.log('phoneNumber: ' + this.phoneNumber);
        console.log(order);
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${OrdersComponent.getDismissReason(reason)}`;
        });
    }
}
