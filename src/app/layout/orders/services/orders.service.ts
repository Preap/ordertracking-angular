import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppModule } from '../../../app.module';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {

  serverRoot = this.app.BACKEND_URL;
  restaurantID = this.app.RESTAURANT_ID;

  constructor(
    private http: HttpClient,
    private app: AppModule
  ) { }

  getOrders(): Observable<any> {
    const getRoute = '/order/getOrders/';
    const url = this.serverRoot + getRoute + this.restaurantID;
    return this.http.get(url);
  }

  assignOrder(orderID: number, vehicleID: number, driverID: number): Observable<any> {
    const assignOrderRoute = '/order/assignDriverAndVehicle';
    const url = this.serverRoot + assignOrderRoute;

    const body = {
      'order_id': orderID,
      'vehicle_id': vehicleID,
      'driver_id': driverID
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
      })
    };

    return this.http.post(url, body, httpOptions);
  }

  changeOrderStatus(orderID: number, status: string): Observable<any> {
    const changeOrderStatusRoute = '/order/changeStatus';
    const url = this.serverRoot + changeOrderStatusRoute;

    const body = {
      'status': status,
      'order_id': orderID
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
      })
    };

    return this.http.post(url, body, httpOptions);
  }
}
