export class LatLng {

    lat: number;
    lng: number;


    constructor(lat: number, lng: number) {
        this.lat = lat;
        this.lng = lng;
    }

    public LatLngDistance(to: LatLng) {
        const er = 6366.707;

        const latFrom = this.deg2rad(this.lat);
        const latTo   = this.deg2rad(to.lat);
        const lngFrom = this.deg2rad(this.lng);
        const lngTo   = this.deg2rad(to.lng);

        const x1 = er * Math.cos(lngFrom) * Math.sin(latFrom);
        const y1 = er * Math.sin(lngFrom) * Math.sin(latFrom);
        const z1 = er * Math.cos(latFrom);

        const x2 = er * Math.cos(lngTo) * Math.sin(latTo);
        const y2 = er * Math.sin(lngTo) * Math.sin(latTo);
        const z2 = er * Math.cos(latTo);

        const d = Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2) + (z1 - z2) * (z1 - z2));

        return d;
    }

    deg2rad(x) {
        return x * (Math.PI / 180);
    }
}
