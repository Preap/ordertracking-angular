import {Injectable} from '@angular/core';
import {AppModule} from '../../../app.module';
import {HttpClient} from '@angular/common/http';
import {Order} from '../Order';
import {Driver} from '../../drivers/Driver';
import {Vehicle} from '../../vehicles/Vehicle';
import {OrdersService} from './orders.service';
import {DriversService} from '../../drivers/services/drivers.service';
import {LatLng} from './LatLng';
import {connectableObservableDescriptor} from 'rxjs/internal/observable/ConnectableObservable';

@Injectable({
    providedIn: 'root'
})
export class AutomaticAssignmentService {
     availableDrivers: Driver[] = [];
     availableVehicles: Vehicle[] = [];
     unassignedOrders: Order[] = [];
    driverIndex = 0;

    constructor(
        private http: HttpClient,
        private app: AppModule,
        private ordersService: OrdersService,
        private driverService: DriversService
    ) {
    }

    deliveryRoute: Order[] = [];

    serverRoot = this.app.BACKEND_URL;
    restaurantID = this.app.RESTAURANT_ID;
    selectedDriver: Driver;
    // selectedVehicle: Vehicle;
    // selectedOrders: Order[] = [];
    acceptableDistance = this.app.ACCEPTABLE_DISTANCE_BETWEEN_ORDERS;
    maxOrders = this.app.ACCEPTABLE_AMOUNT_OF_ORDERS;
    assignedOrdersAmount: number;


    assignOrders(orderList: Order[], driverList: Driver[], vehicleList: Vehicle[]) {
        this.selectedDriver = driverList[0];
        this.driverService.changeDriverAvailability(this.selectedDriver.id, false)
            .subscribe(response => {
                console.log(response);
                this.assignedOrdersAmount = 0;
                this.deliveryRoute = [];
                for (const order of orderList) {
                    if (this.assignedOrdersAmount < 1) {
                        this.assignedOrdersAmount++;
                        this.deliveryRoute.push(order);
                        this.ordersService.assignOrder(order.id, vehicleList[0].id, this.selectedDriver.id)
                            .subscribe(resp => {
                                console.log(resp);
                            });
                    } else {
                        const acceptable = this.checkDeliveryRouteCompatibility(this.deliveryRoute, order);
                        if (acceptable) {
                            this.deliveryRoute.push(order);
                            this.assignedOrdersAmount++;
                            this.ordersService.assignOrder(order.id, vehicleList[0].id, this.selectedDriver.id)
                                .subscribe(res => {
                                    console.log(res);
                                });
                        }
                    }
                }
            });
    }

    checkDeliveryRouteCompatibility(deliveryRoute: Order[], order): boolean {
        for (const confirmedOrder of deliveryRoute) {
            const distance = this.getDistanceBetweenOrders(confirmedOrder, order);
            if (distance < this.acceptableDistance && deliveryRoute.length <= this.maxOrders) {
                deliveryRoute.push(order);
                return true;
            }
        }
        return false;
    }

    // noinspection JSMethodCanBeStatic
    private getDistanceBetweenOrders(existingOrder: Order, nextOrder: Order): number {
        const order1 = new LatLng(existingOrder.address.lat, existingOrder.address.lng);
        const order2 = new LatLng(nextOrder.address.lat, nextOrder.address.lng);
        return order1.LatLngDistance(order2);
    }
}
