import {Component, OnInit} from '@angular/core';
import {routerTransition} from '../../router.animations';
import {DriversService} from '../drivers/services/drivers.service';
import {Driver} from '../drivers/Driver';
import {MapsService} from './maps.service';
import {DeliveryListService} from '../../deliveryList/deliveryList.service';
import {Order} from '../orders/Order';
import {Waypoint} from '../../deliveryList/Waypoint';

@Component({
    selector: 'app-maps',
    templateUrl: './maps.component.html',
    styleUrls: ['./maps.component.scss'],
    animations: [routerTransition()]
})

export class MapsComponent implements OnInit {

    title = 'Maps';
    lat = 35.138493;  // initial lat
    lng = 33.356504;  // initial lng
    zoom = 12;        // initial zoom
    private showMarker = false;
    private markerLat: number;
    private markerLng: number;
    private origin: any;
    private restaurantLat: number;
    private restaurantLng: number;
    private destination: any;
    private waypoints: object;
    private optimizeWaypoints: boolean; // default: true
    private markerOptions: any;
    private renderOptions: any;
    drivers: Driver[];
    deliveryList: Waypoint[] = [];

    constructor(
        private driverService: DriversService,
        private mapsService: MapsService,
        private deliveryListService: DeliveryListService
    ) {
    }

    ngOnInit(): void {
        // this.deliveryList = [];
        this.getDrivers();
        this.getRestaurantCoords();
    }

    putMarker($event: MouseEvent) {
        // @ts-ignore
        this.markerLat = $event.coords.lat;
        // @ts-ignore
        this.markerLng = $event.coords.lng;
        this.showMarker = true;
    }

    getDirection() {
        this.origin = {
            lat: this.restaurantLat,
            lng: this.restaurantLng
        };
        this.destination = this.origin;
        this.optimizeWaypoints = true;
        this.markerOptions = {
            origin: {
                infoWindow: 'This is origin.',
                icon: 'your-icon-url',
                draggable: true,
            },
            destination: {
                icon: 'your-icon-url',
                opacity: 0,
            },
        };
        this.renderOptions = {
            suppressMarkers: false,
        };
        this.waypoints = this.deliveryList;
    }

    getWaypoints(driver_id) {
        this.deliveryList = [];
        this.deliveryListService.getDeliveryList(driver_id)
            .subscribe((orders: Order[]) => {
                for (const order of orders) {
                    const lat = order.address.lat;
                    const lng = order.address.lng;
                    const waypoint = new Waypoint({lat, lng}, true);
                    this.deliveryList.push(waypoint);
                    this.getDirection();
                }
            });
    }

    getDrivers() {
        this.driverService.getDrivers()
            .subscribe(drivers => {
                this.drivers = drivers;
            });
    }

    getRestaurantCoords(): void {
        this.mapsService.getRestaurant()
            .subscribe(restaurant => {
                this.restaurantLat = restaurant.lat;
                this.restaurantLng = restaurant.lng;
            });
    }

    printWaypoints() {
        console.log(this.deliveryList);
    }
}
