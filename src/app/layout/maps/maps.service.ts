import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { AppModule} from '../../app.module';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class MapsService {
    serverRoot = this.app.BACKEND_URL;
    restaurantID = this.app.RESTAURANT_ID;

    constructor(
        private http: HttpClient,
        private app: AppModule
    ) { }

    getRestaurant(): Observable<any> {
        const route = '/restaurant/get/';
        const url = this.serverRoot + route + this.restaurantID;
        return this.http.get(url);
    }
}
