import { NgModule} from '@angular/core';
import { CommonModule} from '@angular/common';

import { MapsRoutingModule } from './maps-routing.module';
import { MapsComponent } from './maps.component';
import { PageHeaderModule} from '../../shared';
import { AgmCoreModule} from '@agm/core';
import { AgmDirectionModule} from 'agm-direction';
import {DriversService} from '../drivers/services/drivers.service';

@NgModule({
  imports: [
    CommonModule,
    MapsRoutingModule,
    PageHeaderModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBquC_NQpsUwCqtxsxVGJDJS_o2U4dE3XE'
    }),
      AgmDirectionModule,
  ],
  declarations: [MapsComponent],
  providers: [DriversService]
})

export class MapsModule {
}
