import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { AppModule } from '../../../app.module';

@Injectable({
  providedIn: 'root'
})



export class DriversService {
  serverRoot = this.app.BACKEND_URL;
  restaurantID = this.app.RESTAURANT_ID;

  constructor(private http: HttpClient,
              private app: AppModule) { }

  getDrivers(): Observable<any> {
    const route = '/driver/getDrivers/';
    const url = this.serverRoot + route + this.restaurantID;
    return this.http.get(url);
  }

  getDriver(driverID): Observable<any> {
    const route = '/getDriver/'
    const url = this.serverRoot + route + driverID;
    return this.http.get(url);
  }

  changeDriverAvailability(driverID: number, availability: boolean): Observable<any> {
    const route = '/driver/changeAvailability';
    const url = this.serverRoot + route;

    const body = {
      'driver_id': driverID,
      'availability': availability
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
      })
    };

    return this.http.post(url, body, httpOptions);
  }
}
