import { Component, OnInit} from '@angular/core';
import { routerTransition} from '../../router.animations';
import { Driver } from './Driver';
import { DriversService } from './services/drivers.service';
import { OrdersService } from '../orders/services/orders.service';
import { VehiclesService } from '../vehicles/services/vehicles.service';
import {Order} from '../orders/Order';
import {Vehicle} from '../vehicles/Vehicle';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {FetchOrderAndVehiclesService} from './modal-assing-order-and-vehicle/fetchOrderAndVehicles.service';

@Component({
    selector: 'app-drivers',
    templateUrl: './drivers.component.html',
    styleUrls: ['./drivers.component.scss'],
    animations: [routerTransition()]
})

export class DriversComponent implements OnInit {

    drivers: Driver[];
    orders: Order[];
    vehicles: Vehicle[];
    closeResult: string;
    selectedDriver: any;
    selectedVehicle: any;
    selectedOrder: any;

    constructor(
        private driverService: DriversService,
        private modalService: NgbModal,
        private vehicleService: VehiclesService,
        private orderService: OrdersService
    ) {}

    static parseWord(word) {
        const result = word.split(' ');
        return result[result.length - 1];
    }

    open(content, driverID) {
        this.selectedDriver = driverID;
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }

    ngOnInit() {
        // TODO - make timer to get drivers after X seconds
        this.getDrivers();

        if (!this.orders) { this.getOrders(); }
        if (!this.vehicles) { this.getVehicles(); }
    }

    getDrivers(): void {
        this.driverService.getDrivers()
            .subscribe(drivers => this.drivers = drivers);
    }

    getOrders(): void {
        this.orderService.getOrders()
            .subscribe(orders => this.orders = orders);
    }

    getVehicles(): void {
        this.vehicleService.getVehicles()
            .subscribe(vehicles => this.vehicles = vehicles);
    }

    assignOrder(): void {
        this.selectedVehicle = DriversComponent.parseWord(this.selectedVehicle);
        this.selectedOrder = DriversComponent.parseWord(this.selectedOrder);
        // console.log('selected order: ' + this.selectedOrder);
        // console.log('selected driver: ' + this.selectedDriver);
        // console.log('selected vehicle: ' + this.selectedVehicle);
        this.orderService.assignOrder(this.selectedOrder, this.selectedVehicle, this.selectedDriver)
            .subscribe(response => console.log(response));
    }
}
