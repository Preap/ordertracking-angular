import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppModule } from '../../../app.module';
import { HttpClient } from '@angular/common/http';
import { OrdersService } from '../../orders/services/orders.service';
import { VehiclesService } from '../../vehicles/services/vehicles.service';

@Injectable({
    providedIn: 'root'
})
export class FetchOrderAndVehiclesService {

    constructor(
        private http: HttpClient,
        private app: AppModule,
        private orderService: OrdersService,
        private vehicleService: VehiclesService
    ) { }

    getOrders(): Observable<any> {
        return this.orderService.getOrders();
    }

    getVehicles(): Observable<any> {
        return this.vehicleService.getVehicles();
    }
}
