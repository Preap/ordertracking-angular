import {Component, OnInit} from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Vehicle } from '../../vehicles/Vehicle';
import { Order } from '../../orders/Order';
import { FetchOrderAndVehiclesService } from './fetchOrderAndVehicles.service';

@Component({
    selector: 'app-modal',
    templateUrl: './modal.component.html',
    styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

    orders: Order[];
    vehicles: Vehicle[];

    closeResult: string;
    constructor(private modalService: NgbModal,
                private fetchModalData: FetchOrderAndVehiclesService) { }

    open(content) {
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }

    ngOnInit(): void {
        this.getOrders();
        this.getVehicles();
    }

    getOrders(): void {
        this.fetchModalData.getOrders()
            .subscribe(orders => this.orders = orders);
    }

    getVehicles(): void {
        this.fetchModalData.getVehicles()
            .subscribe(vehicles => this.vehicles = vehicles);
    }
}
