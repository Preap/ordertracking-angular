import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DriversRoutingModule} from './drivers-routing.module';
import { DriversComponent } from './drivers.component';
import { PageHeaderModule } from '../../shared';
import { ModalComponent } from './modal-assing-order-and-vehicle/modal.component';

@NgModule({
    imports: [CommonModule, DriversRoutingModule, PageHeaderModule, FormsModule, NgbModule],
    declarations: [DriversComponent, ModalComponent],
})

export class DriversModule {}
