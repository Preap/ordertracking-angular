export class Driver {
    id: number;
    firstName: string;
    lastName: string;
    phone: number;
    status: string;
    availability: boolean;
    currentLatitude: number|null;
    currentLongitude: number|null;


    constructor(id: number, firstName: string, lastName: string, phone: number,
                status: string, availability: boolean, currentLatitude: number, currentLongitude: number) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
        this.status = status;
        this.availability = availability;
        this.currentLatitude = currentLatitude;
        this.currentLongitude = currentLongitude;
    }
}
