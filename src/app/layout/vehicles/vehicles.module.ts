import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VehiclesRoutingModule} from './vehicles-routing.module';
import { VehiclesComponent } from './vehicles.component';
import { PageHeaderModule } from '../../shared';

@NgModule({
    imports: [CommonModule, VehiclesRoutingModule, PageHeaderModule],
    declarations: [VehiclesComponent]
})

export class VehiclesModule {}
