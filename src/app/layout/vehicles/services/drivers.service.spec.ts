import { TestBed } from '@angular/core/testing';

import { VehiclesService } from './drivers.service';

describe('DriversService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VehiclesService = TestBed.get(VehiclesService);
    expect(service).toBeTruthy();
  });
});
