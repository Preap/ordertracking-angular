import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { AppModule } from '../../../app.module';

@Injectable({
  providedIn: 'root'
})
export class VehiclesService {

  serverRoot = this.app.BACKEND_URL;
  restaurantID = this.app.RESTAURANT_ID;
  route = '/vehicle/getVehicles/';

  constructor(private http: HttpClient,
              private app: AppModule) { }

  getVehicles(): Observable<any> {
    const url = this.serverRoot + this.route + this.restaurantID;
    return this.http.get(url);
  }
}
