import { Component, OnInit} from '@angular/core';
import { routerTransition} from '../../router.animations';
import { Vehicle } from './Vehicle';
import { VehiclesService } from './services/vehicles.service';

@Component({
    selector: 'app-vehicles',
    templateUrl: './vehicles.component.html',
    styleUrls: ['./vehicles.component.scss'],
    animations: [routerTransition()]
})

export class VehiclesComponent implements OnInit {

    vehicles: Vehicle[];

    constructor(
        private vehiclesService: VehiclesService
    ) {}

    ngOnInit() {
        this.getVehicles();
    }

    getVehicles(): void {
        this.vehiclesService.getVehicles()
            .subscribe(vehicles => this.vehicles = vehicles);
    }
}
