export class Vehicle {
  id: number;
  type: string;
  currentCapacity: number;
  maxCapacity: number;
  availability: boolean;
  status: string;
  driver: string;


  constructor(id: number, type: string, currentCapacity: number,
              maxCapacity: number, availability: boolean, status: string, driver: string) {
    this.id = id;
    this.type = type;
    this.currentCapacity = currentCapacity;
    this.maxCapacity = maxCapacity;
    this.availability = availability;
    this.status = status;
    this.driver = driver;
  }
}
