import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';

import { DriverLayoutRoutingModule } from './driver-layout-routing.module';
import { DriverLayoutComponent } from './driver-layout.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { HeaderComponent } from './header/header.component';
// import { DriverMapComponent } from './driver-map/driver-map.component';
// import { DriverViewOrdersComponent } from './driver-view-orders/driver-view-orders.component';


@NgModule({
    imports: [
        CommonModule,
        DriverLayoutRoutingModule,
        TranslateModule,
        NgbDropdownModule
    ],
    declarations: [DriverLayoutComponent, SidebarComponent, HeaderComponent]
})

export class DriverLayoutModule {}
