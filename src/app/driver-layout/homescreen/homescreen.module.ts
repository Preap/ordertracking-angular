import { NgModule} from '@angular/core';
import { CommonModule} from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { HomescreenRoutingModule } from './homescreen-routing.module';
import { HomescreenComponent } from './homescreen.component';
import { PageHeaderModule } from '../../shared/modules';
import { StatModule } from '../../shared';

@NgModule({
    imports: [CommonModule, HomescreenRoutingModule, PageHeaderModule, NgbModule, StatModule],
    declarations: [HomescreenComponent]
})

export class HomescreenModule {}
