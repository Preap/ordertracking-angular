import { Component, OnInit, OnDestroy } from '@angular/core';
import { routerTransition } from '../../router.animations';
import {AppModule} from '../../app.module';
import { DriversService } from '../../layout/drivers/services/drivers.service';
import {Driver} from '../../layout/drivers/Driver';

@Component({
    selector: 'app-driver-homescreen',
    templateUrl: './homescreen.component.html',
    styleUrls: ['./homescreen.component.scss'],
    animations: [routerTransition()]
})

export class HomescreenComponent implements OnInit {

    driver: Driver;

    ngOnInit(): void {
        this.getDriver(Number(localStorage.getItem('DRIVER_ID')));
    }
    constructor(
        private app: AppModule,
        private driverService: DriversService
    ) {}

    getDriver(driverID: number) {
        this.driverService.getDriver(driverID).subscribe(driver => {
            this.driver = driver;
        });
    }
}
