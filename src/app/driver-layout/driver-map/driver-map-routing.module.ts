import { NgModule} from '@angular/core';
import { Routes, RouterModule} from '@angular/router';
import { DriverMapComponent } from './driver-map.component';

const  routes: Routes = [
    {
        path: '',
        component: DriverMapComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class DriverMapRoutingModule {}
