import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { AppModule} from '../../app.module';
import { HttpClient } from '@angular/common/http';
import {Driver} from '../../layout/drivers/Driver';

@Injectable({
    providedIn: 'root'
})

export class DriverMapService {

    serverRoot = this.app.BACKEND_URL;

    constructor(
        private app: AppModule,
        private http: HttpClient
    ) {}

    getDriverObject(driverID): Observable<any> {
        const route = '/getDriver/';
        const url = this.serverRoot + route + driverID;
        return this.http.get(url);
    }
}
