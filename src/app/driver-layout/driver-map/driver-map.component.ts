import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import {AppModule} from '../../app.module';

import { AgmCoreModule } from '@agm/core';
import {DeliveryListService} from '../../deliveryList/deliveryList.service';
import {Order} from '../../layout/orders/Order';
import {Waypoint} from '../../deliveryList/Waypoint';
import {Driver} from '../../layout/drivers/Driver';
import {DriverMapService} from './driver-map.service';

@Component({
  selector: 'app-driver-map',
  templateUrl: './driver-map.component.html',
  styleUrls: ['./driver-map.component.scss'],
  animations: [routerTransition()]
})
export class DriverMapComponent implements OnInit {

  constructor(
      private agmMaps: AgmCoreModule,
      private deliveryListService: DeliveryListService,
      private driverMapService: DriverMapService
  ) { }

  driverID = localStorage.getItem('DRIVER_ID');
  driver: any;
  private showMarker = false;
  private markerLat: number;
  private markerLng: number;
  private origin: any;
  private restaurantLat: number;
  private restaurantLng: number;
  private destination: any;
  private waypoints: object;
  private optimizeWaypoints: boolean; // default: true
  private markerOptions: any;
  private renderOptions: any;

  lat = 35.138493;  // initial lat
  lng = 33.356504;  // initial lng
  zoom = 12;
  private deliveryList: any[];

  ngOnInit() {
    this.getDriverObject();
    this.getDeliveryList();
  }

  getDeliveryList() {
    this.deliveryList = [];
    const DRIVER_ID: number = Number(localStorage.getItem('DRIVER_ID'));
    this.deliveryListService.getDeliveryList(DRIVER_ID)
        .subscribe((orders: Order[]) => {
          for (const order of orders) {
            const lat = order.address.lat;
            const lng = order.address.lng;
            const waypoint = new Waypoint({lat, lng}, true);
            this.deliveryList.push(waypoint);
          }
        });
  }

  async getDriverObject() {
    this.driverMapService.getDriverObject(this.driverID)
        .subscribe(driver => {
          this.driver = driver;
          this.restaurantLat = driver['restaurant']['lat'];
          this.restaurantLng = driver['restaurant']['lng'];
        });
  }

  getDirection() {
    this.origin = {
      lat: this.restaurantLat,
      lng: this.restaurantLng
    };
    this.destination = this.origin;
    this.optimizeWaypoints = true;
    // this.markerOptions = {
    //   origin: {
    //     infoWindow: 'This is origin.',
    //     icon: 'your-icon-url',
    //     draggable: true,
    //   },
    //   destination: {
    //     icon: 'your-icon-url',
    //     label: 'marker label',
    //     opacity: 0.8,
    //   },
    // };
    this.renderOptions = {
      suppressMarkers: false,
    };
    this.waypoints = this.deliveryList;
  }

}
