import { NgModule} from '@angular/core';
import { CommonModule} from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { DriverMapComponent } from './driver-map.component';
import { DriverMapRoutingModule } from './driver-map-routing.module';
import { PageHeaderModule } from '../../shared/modules';
import { AgmCoreModule} from '@agm/core';
import { AgmDirectionModule} from 'agm-direction';

@NgModule({
    imports: [
        CommonModule,
        DriverMapRoutingModule,
        PageHeaderModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyBquC_NQpsUwCqtxsxVGJDJS_o2U4dE3XE'
        }),
        AgmDirectionModule,
    ],
    declarations: [DriverMapComponent]
})

export class DriverMapModule {}
