import { NgModule} from '@angular/core';
import { CommonModule} from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { DriverViewOrdersRoutingModule } from './driver-view-orders-routing.module';
import { DriverViewOrdersComponent } from './driver-view-orders.component';
import { PageHeaderModule } from '../../shared/modules';

@NgModule({
    imports: [CommonModule, DriverViewOrdersRoutingModule, PageHeaderModule, NgbModule],
    declarations: [DriverViewOrdersComponent]
})

export class DriverViewOrdersModule {}
