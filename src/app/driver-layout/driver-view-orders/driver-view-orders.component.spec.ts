import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DriverViewOrdersComponent } from './driver-view-orders.component';

describe('DriverViewOrdersComponent', () => {
  let component: DriverViewOrdersComponent;
  let fixture: ComponentFixture<DriverViewOrdersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DriverViewOrdersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DriverViewOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
