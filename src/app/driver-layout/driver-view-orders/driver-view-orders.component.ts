import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import {AppModule} from '../../app.module';

import { DeliveryListService } from '../../deliveryList/deliveryList.service';
import { OrdersService } from '../../layout/orders/services/orders.service';
import { DriversService } from '../../layout/drivers/services/drivers.service';
import {Order} from '../../layout/orders/Order';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-driver-view-orders',
  templateUrl: './driver-view-orders.component.html',
  styleUrls: ['./driver-view-orders.component.scss'],
  animations: [routerTransition()]
})
export class DriverViewOrdersComponent implements OnInit {

  private ordersList: Order[] = [];
  closeResult: string;
  private selectedOrder: number;
  shownOrder: Order;
  phoneNumber: number;
  driverID: number;

  private static getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  constructor(
      private app: AppModule,
      private modalService: NgbModal,
      private deliveryListService: DeliveryListService,
      private ordersService: OrdersService,
      private driverService: DriversService
  ) { }

  ngOnInit() {
    this.getDeliveryList();
    this.driverID = +window.localStorage.getItem('DRIVER_ID');
  }

  getDeliveryList() {
    this.ordersList = [];
    const driverID: string = localStorage.getItem('DRIVER_ID');
    this.deliveryListService.getDeliveryList(Number(driverID))
        .subscribe((orders: Order[]) => {
          for (const order of orders) {
            const nextOrder = new Order(order.id, order.type, order.timestamp, order.driver, order.address, order.status, order.customer, order.items);
            if (nextOrder.status !== 'completed') {
              this.ordersList.push(nextOrder);
            }
          }
        });
  }

  open(content, orderID) {
    this.selectedOrder = orderID;
      this.modalService.open(content).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${DriverViewOrdersComponent.getDismissReason(reason)}`;
      });
  }

  openOrderModalData(content, order) {
    this.shownOrder = order;
    this.phoneNumber = parseInt(order.customer.phone);
    console.log('phoneNumber: ' + this.phoneNumber);
    console.log(order);
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${DriverViewOrdersComponent.getDismissReason(reason)}`;
    });
  }

  changeOrderStatus(status: string) {
    console.log(this.selectedOrder + ' changed to: ' + status);
    this.ordersService.changeOrderStatus(this.selectedOrder, status)
        .subscribe(response => {
          console.log(response);
          this.getDeliveryList();
          this.modalService.dismissAll();
        });
  }

  test(id) {
    console.log(id);
  }

  changeDriverAvailability(driverID: number, availability: boolean) {
    console.log(driverID);
    console.log(availability);

    this.driverService.changeDriverAvailability(driverID, availability)
        .subscribe(response => {
          console.log(response);
        });

    for (const order of this.ordersList) {
      if (order.status === 'preparing') {
        this.ordersService.changeOrderStatus(order.id, 'delivering')
            .subscribe(response => {
              console.log(response);
            });
      }
    }
  }
}
