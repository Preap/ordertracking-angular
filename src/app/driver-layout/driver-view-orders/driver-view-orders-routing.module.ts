import { NgModule} from '@angular/core';
import { Routes, RouterModule} from '@angular/router';
import { DriverViewOrdersComponent } from './driver-view-orders.component';

const  routes: Routes = [
    {
        path: '',
        component: DriverViewOrdersComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class DriverViewOrdersRoutingModule {}
