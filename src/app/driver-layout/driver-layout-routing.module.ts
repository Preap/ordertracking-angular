import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DriverLayoutComponent } from './driver-layout.component';

const routes: Routes = [
    {
        path: '',
        component: DriverLayoutComponent,
        children: [
            { path: '', redirectTo: 'dashboard'},
            { path: 'map', loadChildren: './driver-map/driver-map.module#DriverMapModule'},
            { path: 'dashboard', loadChildren: './homescreen/homescreen.module#HomescreenModule'},
            { path: 'orders', loadChildren: './driver-view-orders/driver-view-orders.module#DriverViewOrdersModule'},
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DriverLayoutRoutingModule {}
