import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppModule } from '../app.module';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Driver} from '../layout/drivers/Driver';

@Injectable({
    providedIn: 'root'
})

export class DeliveryListService {
    serverRoot = this.app.BACKEND_URL;
    restaurantID = this.app.RESTAURANT_ID;

    constructor(
        private http: HttpClient,
        private app: AppModule
    ) { }

    getDeliveryList(driver: number) {
        const getDeliveryListRoute = '/order/getDeliveryList/';
        const url = this.serverRoot + getDeliveryListRoute + driver;

        return this.http.get(url);
    }
}
