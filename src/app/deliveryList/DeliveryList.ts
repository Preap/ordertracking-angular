import {Driver} from '../layout/drivers/Driver';
import {Order} from '../layout/orders/Order';

export class DeliveryList {
    driver: Driver;
    orders: Order[];
}
