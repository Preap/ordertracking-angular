export class Waypoint {
        location: {
            lat: number,
            lng: number,
        };
        stopover: boolean;


    constructor(location: { lat: number; lng: number }, stopover: boolean) {
        this.location = location;
        this.stopover = stopover;
    }
}
